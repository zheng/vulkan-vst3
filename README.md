# Vulkan VST3/VST-GUI4 Plug-in
## Spectrogram
### 2020-2021
**A collabaration between ENSICAEN's students and Steinberg Media Technologies**

*Dev. team* :
- Pierre Buée (Audio processing)
- Valentin Deleval (Vulkan)
- Benjamin Samuth (VST3/VST-GUI4 integration)
- Christian (Vulkan)

## Requirements
- [Vulkan Packages](https://vulkan.lunarg.com/sdk/home) (Tools + SDK)
- [Intel Integrated Performance Primitives](https://software.intel.com/content/www/us/en/develop/tools/oneapi/components/ipp.html)
    - *Note: you will have to set the environment variable IPPROOT to your installation directory, or modify the CMakeLists.txt*

## Optional
- [Steinberg's VST3 Audio Plug-in SDK](https://new.steinberg.net/developers/) TestHost to use the Plugin if you don't already have a host program. You can use any DAW that supports VST3 otherwise.

## Supported Platforms
The 1.0.0 release is only stable on Windows 64-bit.
There is a Linux-support version on the branch `linux-x11`, but the plug-in is still buggy.

## Set up & Build
Run CMake (>= 3.16) on the `CMakeLists.txt` file in the root directory of the project to set up a build directory.
Run `make` in this new directory.
To use the plug-in, you will have to copy the directory `VST3/vulkanVSTPlugin.vst3` and paste it in one of the default location for VST3 plugins such as `auiePrograms\\Common Files\\VST3` on Windows or `~/.vst3/` on Linux if it's not automatically done. Then you can run your host and try it.
