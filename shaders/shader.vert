#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 0) uniform UniformBufferObject {
    mat4 model;
    mat4 view;
    mat4 proj;
} ubo;

layout(binding = 2) uniform sampler2D heightMapSampler;

layout(location = 0) in vec2 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec2 texCoord;

layout(location = 0) out vec2 uv;

layout(location = 1) out vec3 normal;
layout(location = 2) out vec3 lightDirection;


const float offset = 0.5;
const float d = 1./1024.;
const float MAX_MAGNITUDE = 16;

const float log100 = 2.0;
const float log200 = 2.301029995663981;

vec4 magnitude(in vec2 coord) {
    coord.x = pow(10, log200*coord.x+log100)/20000;
    return texture(heightMapSampler, coord);
}

float gray(const in vec4 c) {
    return log(c.r+1)/log(MAX_MAGNITUDE);
}

vec3 computeNormal() {
    vec2 texcoord = inPosition + vec2(offset, offset);
    float p01 = gray(magnitude(texcoord + vec2(-d, 0)));
    float p10 = gray(magnitude(texcoord + vec2(0, -d)));
    float p21 = gray(magnitude(texcoord + vec2(d, 0)));
    float p12 = gray(magnitude(texcoord + vec2(0, d)));
    float p11 = gray(magnitude(texcoord));

    vec3 u = normalize(vec3(2*d, 0, p21 - p01));
    vec3 v = normalize(vec3(0, 2*d, p12 - p10));
    vec3 n = normalize(cross(u,v));
    return n;
}

vec3 transformNormal(const in mat4 modelWorld, const in vec3 normalInObject)
{
    vec3 normalInWorld = inverse(transpose(mat3(modelWorld))) * normalInObject;
    return normalize(normalInWorld);
}


void main() {
    vec4 color = magnitude(inPosition.xy+offset);
    float z = gray(color);
    mat4 modelWorld = ubo.model;

    vec4 vertexPosition = modelWorld * vec4(inPosition, z, 1.0);
    gl_Position = ubo.proj * ubo.view * vertexPosition;

    uv = vec2(z, 0 /*invariant*/);

    /* >>Light shading not consistent<<
    if (ubo.model[2][2] == 0) {
        //Flat surface -> no need to compute normal vectors
        normal = vec3(0,0,1);
    } else {
        normal = transformNormal(modelWorld, computeNormal());
    }

    vec4 cameraPosition = inverse(ubo.view) * vec4(0,0,0,1);
    lightDirection = normalize(cameraPosition.xyz - vertexPosition.xyz);
    */
}