#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 1) uniform sampler2D colorMapSampler;

layout(location = 0) in vec2 uv;

layout(location = 1) in vec3 normal;
layout(location = 2) in vec3 lightDirection;

layout(location = 0) out vec4 outColor;

vec3 computeLightLambert(const in vec3 diffuse, const in vec3 normal)
{
    return diffuse*max(0.4,dot(lightDirection, normal));
}

void main() {
    vec3 diffuse = texture(colorMapSampler, uv).rgb;
    //vec3 lambert = computeLightLambert(diffuse, normal);
    outColor = vec4(diffuse, 1.0);
}