#ifndef VULKANVST3_VULKANFACADE_HPP
#define VULKANVST3_VULKANFACADE_HPP

#include "iplatformvulkan.hpp"
#define GLM_FORCE_RADIANS
#include <optional>
#include <vector>
#include <string>
#include <vulkan/vulkan.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <array>
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define STB_IMAGE_IMPLEMENTATION

#include <unordered_map>

class VulkanFacade {
private:
    std::unordered_map<std::string, float> _args;
public:
	std::vector<float> &_colormap;
	void setArg(const std::string &key, float value) {_args[key] = value;}
	void setArg(const std::string &key, float *value);


public:
	explicit VulkanFacade(IPlatformVulkan* platform);
	~VulkanFacade();

	void initVulkan();
    void drawFrame();
	void resize(int left, int top, int width, int height);
	void invalidate(int left, int top, int width, int height);
    void waitIdle();
	void cleanup();

	struct SwapChainSupportDetails {
		VkSurfaceCapabilitiesKHR capabilities;
		std::vector<VkSurfaceFormatKHR> formats;
		std::vector<VkPresentModeKHR> presentModes;
	};
	struct Vertex {
		glm::vec3 pos;
		glm::vec3 color;
		glm::vec2 texCoord;

		static VkVertexInputBindingDescription getBindingDescription() {
			VkVertexInputBindingDescription bindingDescription{};
			bindingDescription.binding   = 0;
			bindingDescription.stride    = sizeof(Vertex);
			bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

			return bindingDescription;
		}

		static std::array<VkVertexInputAttributeDescription, 3> getAttributeDescriptions() {
			std::array<VkVertexInputAttributeDescription, 3> attributeDescriptions{};

			attributeDescriptions[0].binding  = 0;
			attributeDescriptions[0].location = 0;
			attributeDescriptions[0].format   = VK_FORMAT_R32G32B32_SFLOAT;
			attributeDescriptions[0].offset   = offsetof(Vertex, pos);

			attributeDescriptions[1].binding  = 0;
			attributeDescriptions[1].location = 1;
			attributeDescriptions[1].format   = VK_FORMAT_R32G32B32_SFLOAT;
			attributeDescriptions[1].offset   = offsetof(Vertex, color);

			attributeDescriptions[2].binding  = 0;
			attributeDescriptions[2].location = 2;
			attributeDescriptions[2].format   = VK_FORMAT_R32G32_SFLOAT;
			attributeDescriptions[2].offset   = offsetof(Vertex, texCoord);

			return attributeDescriptions;
		}
	};

private:
	void createInstance();
	void destroyInstance();

	void pickPhysicalDevice();
	void createLogicalDevice();
	void destroyLogicalDevice();

	void createSurface();
    void destroySurface();

	void createSwapChain();
	void destroySwapChain();
	void recreateSwapChain();
    void cleanupSwapChain();

	void createImageViews();
	void destroyImageViews();

	void createGraphicsPipeline();
	void destroyGraphicsPipeline();

	VkShaderModule createShaderModule(const std::vector<char>& code);
	void destroyShaderModule(const VkShaderModule &module);

	void createRenderPass();
	void destroyRenderPass();

	void createFramebuffers();
	void destroyFramebuffers();

	void createCommandPool();
	void destroyCommandPool();

	void createCommandBuffers();

	void createSyncObjects();
	void destroySyncObjects();

	void setupDebugMessenger();
	void destroyDebugMessenger();
    static void populateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT& createInfo);

	void createVertexBuffer();
	void createIndexBuffer();
	void createUniformBuffers();
	void createDescriptorPool();
	void createDescriptorSets();
	void createDescriptorSetLayout();
	void createBuffer(VkDeviceSize size,
					  VkBufferUsageFlags usage,
					  VkMemoryPropertyFlags properties,
					  VkBuffer &buffer,
					  VkDeviceMemory &bufferMemory);
	void copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size);

	uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties);

	void updateUniformBuffer(uint32_t currentImage);
	void createDepthResources();
	VkFormat findDepthFormat();
	bool hasStencilComponent(VkFormat format);
	VkFormat findSupportedFormat(const std::vector<VkFormat> &candidates, VkImageTiling tiling, VkFormatFeatureFlags features);
	void createImage(uint32_t width,
					 uint32_t height,
					 VkFormat format,
					 VkImageTiling tiling,
					 VkImageUsageFlags usage,
					 VkMemoryPropertyFlags properties,
					 VkImage &image,
					 VkDeviceMemory &imageMemory);
	VkImageView createImageView(VkImage image, VkFormat format, VkImageAspectFlags aspectFlags);

	void createTextures();
	VkCommandBuffer beginSingleTimeCommands();
	void endSingleTimeCommands(VkCommandBuffer commandBuffer);
	void transitionImageLayout(VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout);
	void copyBufferToImage(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height);
	struct QueueFamilyIndices {
		std::optional<uint32_t> graphicsFamily;
		std::optional<uint32_t> presentFamily;

		bool isComplete() { return graphicsFamily.has_value() && presentFamily.has_value(); }
	};

	struct UniformBufferObject {
		alignas(16) glm::mat4 model;
		alignas(16) glm::mat4 view;
		alignas(16) glm::mat4 proj;
	};



	virtual std::vector<const char*> getRequiredExtensions() const;
    virtual void getFramebufferSize(uint32_t &width, uint32_t &height) const;
    virtual void getViewportRect(int32_t &x, int32_t &y, uint32_t &width, uint32_t &height);
	virtual uint8_t * loadTexture(const std::string& filename, int c, int& w, int& h);
    virtual void freeTexture(uint8_t * pixels);
    virtual float * loadTexturef(const std::string& filename, int c, int& w, int& h);
    virtual void freeTexture(float * pixels);
    virtual std::vector<char> readFile(const std::string& filename);

	QueueFamilyIndices findQueueFamilies(const VkPhysicalDevice &device);
	SwapChainSupportDetails querySwapChainSupport(const VkPhysicalDevice &device);
	static VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR> &availableFormats) ;
    static VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR> &availablePresentModes) ;
    VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR &capabilities);
    bool checkValidationLayerSupport();
	bool isDeviceSuitable(const VkPhysicalDevice &device);
	bool checkDeviceExtensionSupport(const VkPhysicalDevice &device);

protected:
	IPlatformVulkan* platformVulkan;

public:
    const std::vector<const char*> validationLayers = {
            "VK_LAYER_KHRONOS_validation"
    };

	#ifdef NDEBUG_VULKAN_VALIDATION_LAYERS
    const bool enableValidationLayers = false;
    #else
    const bool enableValidationLayers = true;
    #endif

	const std::vector<const char*> deviceExtensions = {
        VK_KHR_SWAPCHAIN_EXTENSION_NAME
	};
	std::vector<Vertex> vertices;
    std::vector<uint32_t> indices;
	const int MAX_FRAMES_IN_FLIGHT = 2;

private:
	VkInstance _instance;
	VkPhysicalDevice _physicalDevice = VK_NULL_HANDLE;
	VkDevice _device;
	VkSurfaceKHR _surface;
	VkQueue _graphicsQueue;
	VkQueue _presentQueue;
	VkSwapchainKHR _swapChain{};
	std::vector<VkImage> _swapChainImages;
	VkFormat _swapChainImageFormat;
	VkExtent2D _swapChainExtent{};
	VkRect2D _area{};
	std::vector<VkImageView> _swapChainImageViews;
	VkRenderPass _renderPass;
	VkDescriptorSetLayout _descriptorSetLayout;
    VkPipelineLayout _pipelineLayout;
    VkPipeline _graphicsPipeline;
    std::vector<VkFramebuffer> _swapChainFramebuffers;
	VkDescriptorPool _descriptorPool;
	std::vector<VkDescriptorSet> _descriptorSets;
    VkCommandPool _commandPool;
    std::vector<VkCommandBuffer> _commandBuffers;
    std::vector<VkSemaphore> _imageAvailableSemaphores;
    std::vector<VkSemaphore> _renderFinishedSemaphores;
    std::vector<VkFence> _inFlightFences;
    std::vector<VkFence> _imagesInFlight;
    size_t _currentFrame = 0;

	VkBuffer _vertexBuffer;
	VkDeviceMemory _vertexBufferMemory;
	VkBuffer _indexBuffer;
	VkDeviceMemory _indexBufferMemory;
	std::vector<VkBuffer> _uniformBuffers;
	std::vector<VkDeviceMemory> _uniformBuffersMemory;
	bool _frameBufferResized = false;
	VkImage _depthImage;
	VkDeviceMemory _depthImageMemory;
	VkImageView _depthImageView;
    VkDebugUtilsMessengerEXT _debugMessenger;
	std::vector<float> _textureData;

public:
	struct TextureObject {

		VkSampler m_sampler;
		VkImage m_image;
		VkImageView m_view;
		VkDeviceMemory m_mem;
		VkFormat m_format;
		VkImageLayout m_imageLayout;
		uint32_t m_width;
		uint32_t m_height;
		VkBuffer m_stagingBuffer;
		VkDeviceMemory m_stagingBufferMemory;
		void *m_data;
		size_t m_dataSize;
	};

    void updateTexture(TextureObject & textureObject);
	void destroyTexture(TextureObject & textureObject);

private:
	void initTextureHeightMap(TextureObject &textureObject,
							  uint32_t width,
							  uint32_t height,
							  const std::vector<float> &textureData);
	void createSamplerForHeightMap(TextureObject &textureObject);
    void initTextureColorMap(TextureObject &textureObject, const std::vector<float> &colorMap);
	void createSamplerForColorMap(TextureObject &textureObject);
	VkCommandBuffer beginSingleTimeCommands(VkCommandBufferBeginInfo *beginInfo);

	std::vector<TextureObject> _textures;
};
void generateGridIn(uint32_t nbCellsWidth, std::vector<glm::vec3> &vbo, std::vector<uint32_t> &ibo, float zValue);
std::vector<VulkanFacade::Vertex> setRandomColor(const std::vector<glm::vec3> &grid);

#endif // VULKANVST3_VULKANFACADE_HPP
