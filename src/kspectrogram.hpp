//
// Created by Benjamin on 28/01/2021.
//

#ifndef VULKANVST3_KSPECTROGRAM_HPP
#define VULKANVST3_KSPECTROGRAM_HPP

namespace Spectrogram {
static long SpectrumSize = 1<<13;
static long MaxSpectrums = 1024;
}
#endif // VULKANVST3_KSPECTROGRAM_HPP
