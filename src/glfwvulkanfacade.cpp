#include "glfwvulkanfacade.hpp"

#include "colormaps.hpp"

#include <algorithm>
#include <stdexcept>

GLFWVulkanFacade::GLFWVulkanFacade(GLFWVulkanPlatform* platform) : VulkanFacade(platform)  {
    glfwInit();

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

	window = glfwCreateWindow(720, 720, "Vulkan", nullptr, nullptr);
    glfwSetWindowUserPointer(window, this);
    glfwSetFramebufferSizeCallback(window, framebufferResizeCallback);
	glfwSetCursorPosCallback(window, cursorPositionCallback);
	glfwSetMouseButtonCallback(window, mouseButtonCallback);
	glfwSetScrollCallback(window, scrollCallback);
	platform->setWindow(window);
}

GLFWVulkanFacade::~GLFWVulkanFacade() {
	cleanup();
	glfwDestroyWindow(window);
	glfwTerminate();
}

void GLFWVulkanFacade::framebufferResizeCallback(GLFWwindow* window, int width, int height) {
    auto app = reinterpret_cast<GLFWVulkanFacade*>(glfwGetWindowUserPointer(window));
    app->resize(0, 0, width, height);
}

void GLFWVulkanFacade::cursorPositionCallback(GLFWwindow *window, double xpos, double ypos) {
    auto app = reinterpret_cast<GLFWVulkanFacade*>(glfwGetWindowUserPointer(window));
	if (app->_mouseHoldState) {
		app->_currentCursorPosition = Coord(xpos, ypos);
		Coord drag                  = app->_currentCursorPosition - app->_lastCursorPosition;
		app->_lastCursorPosition    = app->_currentCursorPosition;
		app->_cameraRotationDrag.x += drag.x;
		app->_cameraRotationDrag.y += drag.y;
		app->_cameraRotationDrag.y = std::clamp(app->_cameraRotationDrag.y, 1.0, 179.0);

		app->setArg("xRotationDrag", (float)-app->_cameraRotationDrag.x);
		app->setArg("yRotationDrag", (float)-app->_cameraRotationDrag.y);
	}
}

void GLFWVulkanFacade::scrollCallback(GLFWwindow *window, double xoffset, double yoffset) {
    auto app = reinterpret_cast<GLFWVulkanFacade*>(glfwGetWindowUserPointer(window));
    app->_zoom -= yoffset;
    app->_zoom = std::clamp(app->_zoom, 1.0, 7.0);
    app->setArg("zoom", (float)app->_zoom);
}

void GLFWVulkanFacade::mouseButtonCallback(GLFWwindow *window, int button, int action, int /*mods*/) {
    auto app = reinterpret_cast<GLFWVulkanFacade*>(glfwGetWindowUserPointer(window));
	bool LMBPress = (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS);
    bool LMBRelease = (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE);
	if (LMBPress) {
		double xpos, ypos;
		glfwGetCursorPos(window, &xpos, &ypos);
        app->_currentCursorPosition = Coord(xpos, ypos);
        app->_lastCursorPosition = Coord(xpos, ypos);
        app->_mouseHoldState = true;
        app->setArg("colormap", 3.);
        app->setArg("colormapHasChanged", 1.);
	} else if (LMBRelease) {
        app->_mouseHoldState = false;
        app->setArg("colormap", 0.);
        app->setArg("colormapHasChanged", 1.);
	}
}

//-----------------------------------------------------
std::vector<const char *> GLFWVulkanFacade::getRequiredExtensions() const {
    uint32_t glfwExtensionCount = 0;
    const char** glfwExtensions;
    glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

    std::vector<const char*> extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);

    if (enableValidationLayers) {
        extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
    }

    return extensions;
}


void GLFWVulkanFacade::getFramebufferSize(uint32_t &width, uint32_t &height) const {
	int glfwWidth, glfwHeight;
    glfwGetFramebufferSize(window, &glfwWidth, &glfwHeight);
	width = glfwWidth;
	height = glfwHeight;
}

//-----------------------------------------------------
void GLFWVulkanFacade::runLoop() {
	while (!glfwWindowShouldClose(window)) {
		glfwPollEvents();
		drawFrame();
	}
	waitIdle();
}

//-----------------------------------------------------
VkSurfaceKHR GLFWVulkanPlatform::createSurface(VkInstance instance) {
	VkSurfaceKHR surface;
    if (glfwCreateWindowSurface(instance, window, nullptr, &surface) != VK_SUCCESS) {
        throw std::runtime_error("Failed to create window surface.");
    }
	return surface;
}

const char *GLFWVulkanPlatform::getSurfaceExtension() const { return nullptr; }

void GLFWVulkanPlatform::setWindow(GLFWwindow * glfwWindow) { window = glfwWindow; }

void GLFWVulkanPlatform::changeSize(int left, int top, int width, int height) {

}

void GLFWVulkanPlatform::invalidate(int left, int top, int width, int height) {}

void GLFWVulkanPlatform::getFramebufferSize(int &width, int &height) const {
	width = 720;
	height = 720;
}
