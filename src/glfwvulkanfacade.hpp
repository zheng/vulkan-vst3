#ifndef VULKANVST3_GLFWVULKANFACADE_HPP
#define VULKANVST3_GLFWVULKANFACADE_HPP

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include "vulkanfacade.hpp"


class GLFWVulkanPlatform : public IPlatformVulkan {
public:
    GLFWVulkanPlatform() = default;
    VkSurfaceKHR createSurface(VkInstance instance) override;
    const char *getSurfaceExtension() const override;
	void changeSize(int left, int top, int width, int height) override;
	void invalidate(int left, int top, int width, int height) override;
	void setWindow(GLFWwindow*);
	void getFramebufferSize(int &width, int &height) const override;
	

private:
    GLFWwindow* window;
};

class GLFWVulkanFacade : public VulkanFacade {
public:
	struct Coord {
		double x;
		double y;
		Coord(double x, double y) : x(x), y(y) {}
		Coord() : x(0), y(0) {}
		Coord operator-(const Coord &c) {return Coord(x-c.x, y-c.y);}
	};
public:
	GLFWVulkanFacade(GLFWVulkanPlatform * platform);
	~GLFWVulkanFacade();
	void runLoop();

public:
	std::vector<const char*> getRequiredExtensions() const override;
	void getFramebufferSize(uint32_t &width, uint32_t &height) const override;

    static void framebufferResizeCallback(GLFWwindow* window, int width, int height);
    static void cursorPositionCallback(GLFWwindow* window, double xpos, double ypos);
	static void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
	static void scrollCallback(GLFWwindow* window, double xoffset, double yoffset);

private:
	double _zoom = 2.0f;
    Coord _currentCursorPosition;
    Coord _lastCursorPosition;
    Coord _cameraRotationDrag;
    bool  _mouseHoldState = false;

private:
    GLFWwindow* window;
};

#endif // VULKANVST3_GLFWVULKANFACADE_HPP
