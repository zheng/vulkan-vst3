#ifndef VULKANVST3_IPLATFORMVULKAN_HPP
#define VULKANVST3_IPLATFORMVULKAN_HPP

#include <vulkan/vulkan.h>

class IPlatformVulkan {
public:
	virtual ~IPlatformVulkan() = default;
	virtual VkSurfaceKHR createSurface(VkInstance) = 0;
    virtual const char* getSurfaceExtension() const = 0;
	virtual void getFramebufferSize(int &width, int &height) const = 0;
	virtual void changeSize(int left, int top, int width, int height) = 0;
	virtual void invalidate(int left, int top, int width, int height) = 0;
};
#endif // VULKANVST3_IPLATFORMVULKAN_HPP
