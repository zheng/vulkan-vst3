#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE

#include <glm/mat4x4.hpp>
#include <glm/vec4.hpp>
#include <iostream>
#include "glfwvulkanfacade.hpp"


int main() {
	/* Encapsulate Vulkan initialization functions. */
	GLFWVulkanFacade vulkanFacade(new GLFWVulkanPlatform());

    uint32_t extensionCount = 0;
    vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);
    
    std::cout << extensionCount << " extensions supported\n";

    glm::mat4 matrix;
    glm::vec4 vec;
    auto test = matrix * vec;

	vulkanFacade.initVulkan();
    vulkanFacade.setArg("maxSample", 1.);
    vulkanFacade.setArg("yRotationDrag", 50.);
    vulkanFacade.setArg("xRotationDrag", 0.);
	vulkanFacade.setArg("zoom", 2.);
    vulkanFacade.setArg("colormap", 0.);
    vulkanFacade.setArg("colormapHasChanged", 1.);
    vulkanFacade.runLoop();

    return 0;
}