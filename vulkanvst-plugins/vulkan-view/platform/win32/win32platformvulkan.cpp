#include "win32platformvulkan.hpp"

#include <vstgui/lib/platform/win32/win32frame.h>
#include <vstgui/lib/platform/win32/win32support.h>
#include <iostream>

using namespace VSTGUI;

Win32PlatformVulkan::Win32PlatformVulkan(CVulkanView * view) : _view(view), windowHandle(nullptr), lock() {
    initWindowClass();
    createWindow();
	InitializeCriticalSection(&lock);
}

Win32PlatformVulkan::~Win32PlatformVulkan() {
	DeleteCriticalSection(&lock);
	destroyWindowClass();
    remove();
}

VkSurfaceKHR Win32PlatformVulkan::createSurface(VkInstance instance) {
    VkSurfaceKHR surface;
    VkWin32SurfaceCreateInfoKHR surfaceCreateInfo{};
    surfaceCreateInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
    surfaceCreateInfo.pNext = nullptr;
    surfaceCreateInfo.flags = 0;
	surfaceCreateInfo.hwnd = windowHandle;
	surfaceCreateInfo.hinstance = GetInstance();
	

    if (vkCreateWin32SurfaceKHR(instance, &surfaceCreateInfo, nullptr, &surface) != VK_SUCCESS) {
        throw std::runtime_error("Failed to create surface.");
    }

    return surface;
}


const char *Win32PlatformVulkan::getSurfaceExtension() const {
    return VK_KHR_WIN32_SURFACE_EXTENSION_NAME;
}

void Win32PlatformVulkan::getFramebufferSize(int &width, int &height) const {
    auto scale = (float)_view->getFrame()->getScaleFactor();
    width = scale*_view->getWidth();
	height = scale*_view->getHeight();
}

//---------------------------------------------------------
int32_t Win32PlatformVulkan::instanceCount = 0;
static TCHAR gVulkanWindowClassName[100];

void Win32PlatformVulkan::changeSize(int left, int top, int width, int height) {
	if (windowHandle) {
		EnterCriticalSection(&lock);
        SetWindowPos(windowHandle, HWND_TOP, left, top, width, height,
					 SWP_NOCOPYBITS|SWP_DEFERERASE);
		LeaveCriticalSection(&lock);
	}
}

void Win32PlatformVulkan::invalidate(int left, int top, int width, int height) {
	if (windowHandle) {
        RECT r = {(LONG)left, (LONG)top, (LONG)ceil (left+width), (LONG)ceil (top+height)};
        InvalidateRect (windowHandle, &r, true);
	}
}



void Win32PlatformVulkan::initWindowClass() {
	++instanceCount;
	if (instanceCount == 1) {
		VSTGUI_SPRINTF(gVulkanWindowClassName, TEXT("VSTGUI_Vulkan_%p"), GetInstance());

		WNDCLASS windowClass = {};
		windowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC | CS_GLOBALCLASS; //|CS_OWNDC; // add Private-DC constant

		windowClass.lpfnWndProc = WindowProc;
		windowClass.cbClsExtra  = 0;
		windowClass.cbWndExtra  = 0;
		windowClass.hInstance   = GetInstance();
		windowClass.hIcon       = nullptr;

		windowClass.hCursor       = LoadCursor(NULL, IDC_ARROW);
		windowClass.lpszMenuName  = nullptr;
		windowClass.lpszClassName = gVulkanWindowClassName;
		RegisterClass(&windowClass);
	}
}

void Win32PlatformVulkan::destroyWindowClass() {
	--instanceCount;
	if (instanceCount == 0) { UnregisterClass(gVulkanWindowClassName, GetInstance()); }
}

bool Win32PlatformVulkan::createWindow()
{
    if (windowHandle)
        return false;

    auto * platformFrame = dynamic_cast<Win32Frame *>(_view->getFrame()->getPlatformFrame());
    const CRect &viewRect = _view->getViewSize();
    windowHandle = CreateWindowEx (0, gVulkanWindowClassName, TEXT("Window"),
                                   WS_CHILD | WS_OVERLAPPED | WS_VISIBLE,
                                   viewRect.left, viewRect.top,
                                   viewRect.getWidth(), viewRect.getHeight(),
                                   platformFrame->getHWND(), NULL, GetInstance (), NULL);

    if (windowHandle)
    {
        EnableWindow (windowHandle, false);	// we don't handle mouse and keyboard events
        SetWindowLongPtr (windowHandle, GWLP_USERDATA, (__int3264)(LONG_PTR)this);
		return true;
    }
    return false;

}

void Win32PlatformVulkan::remove()
{
    if (windowHandle)
    {
        SetWindowLongPtr (windowHandle, GWLP_USERDATA, (LONG_PTR)NULL);
        DestroyWindow (windowHandle);
        windowHandle = nullptr;
    }
}

LONG_PTR WINAPI Win32PlatformVulkan::WindowProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    auto* vulkanView = (Win32PlatformVulkan*)(LONG_PTR)GetWindowLongPtr(hwnd, GWLP_USERDATA);
    if (vulkanView)
    {
        switch (message)
        {
            case WM_ERASEBKGND:
            {
                return 1; // don't draw background
            }

            case WM_PAINT:
            {
				vulkanView->_view->vulkanDraw();
                break;
            }
        }
    }
    return DefWindowProc(hwnd, message, wParam, lParam);
}