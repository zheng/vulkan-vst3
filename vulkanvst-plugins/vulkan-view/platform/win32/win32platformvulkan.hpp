#ifndef VULKANVST3_WIN32PLATFORMVULKAN_HPP
#define VULKANVST3_WIN32PLATFORMVULKAN_HPP

#define NOMINMAX
#define VK_USE_PLATFORM_WIN32_KHR
#include "../../../../src/iplatformvulkan.hpp"
#include "../../include/cvulkanview.hpp"

#include <vstgui/lib/cview.h>
#include <vulkan/vulkan.h>
#include <windows.h>

namespace VSTGUI {
class Win32PlatformVulkan: public IPlatformVulkan {
public:
	explicit Win32PlatformVulkan(CVulkanView * view);
	~Win32PlatformVulkan();
	VkSurfaceKHR createSurface(VkInstance) override;
	const char* getSurfaceExtension() const override;
	void getFramebufferSize(int &width, int &height) const override;
	void changeSize(int left, int top, int width, int height) override;
	void invalidate(int left, int top, int width, int height) override;

	void remove();

	static void initWindowClass();
	static void destroyWindowClass();
	bool createWindow();

    static LONG_PTR WINAPI WindowProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);

private:
	CVulkanView * _view;

private:
	HWND windowHandle;
	CRITICAL_SECTION lock;
	static int32_t instanceCount;
};
}
#endif // VULKANVST3_WIN32PLATFORMVULKAN_HPP
