#include "x11platformvulkan.hpp"

#include <iostream>
#include <vstgui4/vstgui/lib/platform/linux/x11platform.h>
#include <vstgui4/vstgui/lib/platform/linux/x11frame.h>
#include <vstgui4/vstgui/lib/platform/platform_x11.h>

using namespace VSTGUI;
using namespace X11;

X11PlatformVulkan::X11PlatformVulkan(CFrame * frame) : _frame(frame) {
}

VkSurfaceKHR X11PlatformVulkan::createSurface(VkInstance instance) {
	VkSurfaceKHR surface;
    VkXcbSurfaceCreateInfoKHR surfaceCreateInfo{};
	surfaceCreateInfo.sType = VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR;
    surfaceCreateInfo.pNext = nullptr;
	surfaceCreateInfo.flags = 0;

	/* Workaround. We need the windowID but the method is not public in X11::Frame. */
	auto x11PlatformFrame = static_cast<IX11Frame *>(dynamic_cast<Frame *>(_frame->getPlatformFrame()));
	surfaceCreateInfo.window = x11PlatformFrame->getX11WindowID();
	surfaceCreateInfo.connection = RunLoop::instance().getXcbConnection();

    if (vkCreateXcbSurfaceKHR(instance, &surfaceCreateInfo, nullptr, &surface) != VK_SUCCESS) {
        throw std::runtime_error("Failed to create surface.");
    }

	return surface;
}

const char *X11PlatformVulkan::getSurfaceExtension() const {
	return VK_KHR_XCB_SURFACE_EXTENSION_NAME;
}
