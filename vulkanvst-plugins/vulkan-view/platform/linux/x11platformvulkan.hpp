#ifndef VULKANVST3_X11PLATFORMVULKAN_HPP
#define VULKANVST3_X11PLATFORMVULKAN_HPP

#define VK_USE_PLATFORM_XCB_KHR
#include "../../../../src/iplatformvulkan.hpp"

#include <vstgui/lib/cframe.h>
#include <vulkan/vulkan.h>
namespace VSTGUI {
namespace X11 {
class X11PlatformVulkan: public IPlatformVulkan {
public:
	explicit X11PlatformVulkan(CFrame *);
	VkSurfaceKHR createSurface(VkInstance) override;
	const char* getSurfaceExtension() const override;

private:
	CFrame *_frame;

};
} // namespace X11
} // namespace VSTGUI

#endif // VULKANVST3_X11PLATFORMVULKAN_HPP
