#ifndef VULKANVST3_MACPLATFORMVULKAN_HPP
#define VULKANVST3_MACPLATFORMVULKAN_HPP

#if MAC_COCOA
#define VK_USE_PLATFORM_MACOS_MVK
#include "../../../../src/iplatformvulkan.hpp"

#include <vstgui/lib/cframe.h>

#include <vulkan/vulkan.h>

namespace VSTGUI {
class MacPlatformVulkan: public IPlatformVulkan {
public:
	explicit MacPlatformVulkan(CFrame *);
	VkSurfaceKHR createSurface(VkInstance) override;
	const char* getSurfaceExtension() const override;

private:
	CFrame * _frame;
};
}
#endif // MAC_COCOA
#endif // VULKANVST3_MACPLATFORMVULKAN_HPP
