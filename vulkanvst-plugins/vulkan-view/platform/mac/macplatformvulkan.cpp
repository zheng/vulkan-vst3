#include "macplatformvulkan.hpp"

// !!! NOT TESTED !!!
// Needs MoltenVK
// Just showing the idea, but there may be more to do to make it compatible

#if MAC_COCOA
using namespace VSTGUI;

MacPlatformVulkan::MacPlatformVulkan(CFrame *frame) : _frame(frame) {}

VkSurfaceKHR MacPlatformVulkan::createSurface(VkInstance) {
    VkMacOSSurfaceCreateInfoMVK createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_MACOS_SURFACE_CREATE_INFO_MVK;
    createInfo.pNext = nullptr;
    createInfo.flags = 0;

	NSViewFrame * nsViewPlatformFrame = dynamic_cast<NSViewFrame *>(_frame->getPlatformFrame());
    createInfo.pView = nsViewPlatformFrame->getNSView();

	if (vkCreateXcbSurfaceKHR(instance, &surfaceCreateInfo, nullptr, &surface) != VK_SUCCESS) {
        throw std::runtime_error("Failed to create surface.");
    }

	return surface;
}

const char* getSurfaceExtension() const {
	return VK_MVK_MACOS_SURFACE_EXTENSION_NAME;
}
#endif // MAC_COCOA