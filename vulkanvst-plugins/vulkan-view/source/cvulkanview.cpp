#include "../include/cvulkanview.hpp"
#include "../include/vulkanfacadeforvstgui.hpp"

#include <vstgui4/vstgui/lib/cdrawcontext.h>
#include <vstgui4/vstgui/lib/cvstguitimer.h>
#include <vstgui4/vstgui/lib/clayeredviewcontainer.h>

#include <iostream>
#include <memory>

using namespace VSTGUI;

void CVulkanView::setVariable(const std::string & key, float value) {
	if (_vulkanFacade)
        _vulkanFacade->setArg(key, value);
}

void CVulkanView::setVariable(const std::string &key, float * value) {
	if (_vulkanFacade) 
        _vulkanFacade->setArg(key, value);
}

CVulkanView::CVulkanView(const CRect &size) : CView(size), _vulkanFacade() {
}

CVulkanView::~CVulkanView() noexcept {
	delete _vulkanFacade;
}

//-----------------------------------------------------------
void CVulkanView::draw(CDrawContext *pContext) {
}

void CVulkanView::setVisible (bool state)
{
    if (state != isVisible ())
    {
        CView::setVisible (state);
        if (isAttached ())
        {
            if (state) {
                startDrawRoutine();
            } else {
                stopDrawRoutine();
            }
        }
    }
}

//-----------------------------------------------------------
void CVulkanView::initView() {
    if (_vulkanFacade == nullptr) {
        _vulkanFacade = new VulkanFacadeForVSTGUI(this);
		getFrame()->registerScaleFactorChangedListeneer(this);
        // Must wait for frame to be initialized before initializing CVulkanView
        _vulkanFacade->initVulkan();
		_vulkanFacade->drawFrame();
		idleRate = FRAME_PER_SECOND;
		setWantsIdle(true);
        startDrawRoutine();
    }
}

void CVulkanView::vulkanDraw() const {
	_vulkanFacade->drawFrame();
}

//------------------------------------------------------------
void CVulkanView::startDrawRoutine() {
	setWantsIdle(true);
}

void CVulkanView::stopDrawRoutine() {
	setWantsIdle(false);
}

void CVulkanView::onIdle() {
	vulkanDraw();
	getParentView()->setDirty();
}

void CVulkanView::setViewSize(const CRect &rect, bool invalid) {
    CView::setViewSize(rect, invalid);
	if (_vulkanFacade) {

        CRect visibleSize (getVisibleViewSize ());
        CPoint offset;
        localToFrame (offset);
        visibleSize.offset (offset.x, offset.y);

        CGraphicsTransform tm;
        tm.scale (_scaleFactor, _scaleFactor);
        tm.transform (visibleSize);

        _vulkanFacade->resize(visibleSize.left, visibleSize.top,
							  visibleSize.getWidth(), visibleSize.getHeight());
        if (invalid) {
            invalidRect(getViewSize());
        }
    }
}

void CVulkanView::parentSizeChanged() {
	CView::parentSizeChanged();
	CRect rect = getViewSize();
    setViewSize(rect, true);
}

void CVulkanView::invalidRect(const CRect &rect) {
	if (_vulkanFacade) {
        _vulkanFacade->invalidate(rect.left, rect.top, rect.getWidth(), rect.getHeight());
    }
}

void CVulkanView::onScaleFactorChanged(CFrame *frame, double newScaleFactor) {
	_scaleFactor = newScaleFactor;
	setViewSize(getVisibleViewSize(), true);
}

bool CVulkanView::checkUpdate(const CRect &updateRect) const {
	return CView::checkUpdate(updateRect);
}

bool CVulkanView::attached(CView *parent) {
	if (CView::attached(parent)) {
		initView();
		return true;
	}
	return false;
}

bool CVulkanView::removed(CView *parent) {
	if (CView::removed(parent)) {
        _vulkanFacade->cleanup();
	}
	return CView::removed(parent);
}

void CVulkanView::beforeDelete() {
    _callback();
}
