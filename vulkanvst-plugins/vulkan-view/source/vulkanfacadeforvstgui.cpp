#include "../include/vulkanfacadeforvstgui.hpp"

#include <vstgui/uidescription/cstream.h>
#include <vstgui/lib/cresourcedescription.h>
#include <vstgui/lib/cstring.h>

using namespace VSTGUI;

uint8_t* VulkanFacadeForVSTGUI::loadTexture(const std::string &filename, int channels, int& w, int& h) {
    CResourceDescription resource(filename.c_str());
	_pixels = new CBitmap(resource);
	w = _pixels->getWidth();
	h = _pixels->getHeight();
	auto access = CBitmapPixelAccess::create(_pixels);
	uint8_t * int8TextureData = access->getPlatformBitmapPixelAccess()->getAddress();

	if (channels == 4) {
		return int8TextureData;
	}

	int size = w*h;
	_int8TextureData = new uint8_t[size*channels];
    for (uint32_t i = 0u; i < size; ++i) {
		for(int c = 0u; c < channels; ++c) {
			_int8TextureData[channels*i + c] = (float)int8TextureData[4u * i + c];
		}
    }
	return _int8TextureData;
}

float* VulkanFacadeForVSTGUI::loadTexturef(const std::string &filename, int channels, int& w, int& h) {
    uint8_t * int8TextureData = loadTexture(filename, channels, w, h);
	int size = w*h;
    _floatTextureData = new float[size*channels];
	// Conversion from unsigned char to float
	for (uint32_t i = 0u; i < size*channels; ++i) {
		_floatTextureData[i] = (float)int8TextureData[i]/255.f;
	}
	return _floatTextureData;
}

void VulkanFacadeForVSTGUI::freeTexture(uint8_t *pixels) {
	if (pixels == _int8TextureData) {
		_pixels->forget();
		_int8TextureData = nullptr;
	}
	delete pixels;
}

void VulkanFacadeForVSTGUI::freeTexture(float *pixels) {
	if (pixels == _floatTextureData) {
		_pixels->forget();
        _floatTextureData = nullptr;
	}
    delete pixels;
}

std::vector<char> VulkanFacadeForVSTGUI::readFile(const std::string& filename) {
    CResourceInputStream inputStream;
    CResourceDescription resource(filename.c_str());
    if (!inputStream.open(resource)) {
        throw std::runtime_error("Failed to read the file.");
    }

    uint32_t fileSize = inputStream.seek(0,SeekableStream::kSeekEnd);
    std::vector<char> buffer(fileSize);
    inputStream.rewind();
    inputStream.readRaw(buffer.data(), fileSize);
    return buffer;
}

void VulkanFacadeForVSTGUI::getFramebufferSize(uint32_t &width, uint32_t &height) const {
	int scaledWidth(_view->getWidth());
	int scaledHeight(_view->getHeight());
	platformVulkan->getFramebufferSize(scaledWidth, scaledHeight);
    width = scaledWidth;
    height = scaledHeight;
}
