#ifndef VULKANVST3_VULKANFACADEFORVSTGUI_HPP
#define VULKANVST3_VULKANFACADEFORVSTGUI_HPP

#include "../../src/vulkanfacade.hpp"
#include "cvulkanview.hpp"

#include <vstgui/lib/cframe.h>
#include <vstgui/lib/crect.h>
#include <vstgui/lib/cbitmap.h>
#include <iostream>

#if _WIN32
#include "../platform/win32/win32platformvulkan.hpp"
#define VULKAN_VST3_PLATFORM_VULKAN(VIEW) new Win32PlatformVulkan(VIEW)
#elif __APPLE__
#include "../platform/mac/macplatformvulkan.hpp"
#define VULKAN_VST3_PLATFORM_VULKAN(VIEW) new MacPlatformVulkan(VIEW)
#elif __linux__
#include "../platform/linux/x11platformvulkan.hpp"
#define VULKAN_VST3_PLATFORM_VULKAN(VIEW) new X11::X11PlatformVulkan(VIEW)
#else
#error "OS not supported!"
#endif


namespace VSTGUI {
class VulkanFacadeForVSTGUI: public VulkanFacade {
public:
    explicit VulkanFacadeForVSTGUI(CVulkanView * view)
            : VulkanFacade(VULKAN_VST3_PLATFORM_VULKAN(view)), _view(view) {}

private:
    uint8_t* loadTexture(const std::string& filename, int c, int& w, int& h) override;
    float* loadTexturef(const std::string& filename, int c, int& w, int& h) override;
    void freeTexture(uint8_t *pixels) override;
    void freeTexture(float *pixels) override;
	std::vector<char> readFile(const std::string& filename) override;
    void getFramebufferSize(uint32_t &width, uint32_t &height) const override;

private:
    CBitmap* _pixels;
	uint8_t* _int8TextureData;
	float * _floatTextureData;
	CVulkanView * _view;
};
}

#endif // VULKANVST3_VULKANFACADEFORVSTGUI_HPP
