#ifndef VULKANVST3_CVULKANVIEW_HPP
#define VULKANVST3_CVULKANVIEW_HPP

#include <vstgui4/vstgui/lib/cview.h>
#include <vstgui/lib/cvstguitimer.h>
#include <vstgui/lib/iscalefactorchangedlistener.h>

class VulkanFacade;
namespace VSTGUI {
class CVulkanView : public CView, public IScaleFactorChangedListener {
public:
    const uint32_t FRAME_PER_SECOND = 120;

public:
	void setVariable(const std::string &key, float value);
	void setVariable(const std::string &key, float * value);
    std::function<void(void)> _callback;

public:
	explicit CVulkanView(const CRect& size);
	~CVulkanView() noexcept override;
	void initView();
	void vulkanDraw() const;

	// CView -----
	void draw(CDrawContext *pContext) override;
    void setVisible(bool state) override;
	void setViewSize(const CRect &rect, bool invalid) override;
	void invalidRect(const CRect &rect) override;
	bool attached(CView *parent) override;
	bool removed(CView *parent) override;
    bool checkUpdate(const CRect &updateRect) const override;
    void beforeDelete() override;

    void onIdle() override;
	void parentSizeChanged() override;

	// IScaleFactorChangedListener -----
	void onScaleFactorChanged(CFrame *frame, double newScaleFactor) override;
private:
    void startDrawRoutine();
    void stopDrawRoutine();

private:
	VulkanFacade * _vulkanFacade;

private:
	double _scaleFactor = 1.0;
};
}

#endif // VULKANVST3_CVULKANVIEW_HPP
