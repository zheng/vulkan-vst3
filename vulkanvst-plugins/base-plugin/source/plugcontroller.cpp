//------------------------------------------------------------------------
// Project     : VulkanVST3
//
// Category    : Examples
// Filename    : plugcontroller.cpp
// Created by  : Steinberg, 01/2018
// Based on    : HelloWorld Example for VST 3
// Description : A Vulkan VST 3 example
//
//-----------------------------------------------------------------------------
// LICENSE
// (c) 2020, Steinberg Media Technologies GmbH, All Rights Reserved
//-----------------------------------------------------------------------------
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//   * Redistributions of source code must retain the above copyright notice, 
//     this list of conditions and the following disclaimer.
//   * Redistributions in binary form must reproduce the above copyright notice,
//     this list of conditions and the following disclaimer in the documentation 
//     and/or other materials provided with the distribution.
//   * Neither the name of the Steinberg Media Technologies nor the names of its
//     contributors may be used to endorse or promote products derived from this 
//     software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
// IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
// OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

#include "../include/plugcontroller.h"

#include "../include/customvulkanview.h"
#include "../include/plugids.h"
#include "pluginterfaces/base/ibstream.h"

#include <base/source/fstreamer.h>
#include <vstgui/lib/controls/clistcontrol.h>
#include <vstgui/lib/controls/cstringlist.h>
#include <vstgui/uidescription/delegationcontroller.h>
#include <vstgui/uidescription/uiattributes.h>

using namespace VSTGUI;

namespace Steinberg::VulkanVST {

class FFTSizeController: public DelegationController {
public:
    explicit FFTSizeController(IController * parent) : DelegationController(parent) {}

    CView *verifyView(CView *view, const UIAttributes &attributes,
                      const IUIDescription *description) override {
        if (auto listControl = dynamic_cast<CListControl*>(view)) {
			auto * listControlDrawer = dynamic_cast<StringListControlDrawer *>(listControl->getDrawer());

			listControlDrawer->setStringProvider([](int32_t row) {
				if (row < 0) {
					return IPlatformString::createWithUTF8String("<Not Defined>");
				} else {
					return IPlatformString::createWithUTF8String(std::to_string( 64u << (uint32_t)row).c_str());
				}
			});
            listControl->setDrawer(listControlDrawer);
        }
        return DelegationController::verifyView(view, attributes, description);
    }
};

class FFTWindowController: public DelegationController {
public:
    explicit FFTWindowController(IController * parent) : DelegationController(parent) {}

    CView *verifyView(CView *view, const UIAttributes &attributes,
                      const IUIDescription *description) override {
        if (auto listControl = dynamic_cast<CListControl*>(view)) {
            auto * listControlDrawer = dynamic_cast<StringListControlDrawer *>(listControl->getDrawer());

            listControlDrawer->setStringProvider([](int32_t row) {
              switch(row) {
				  case 0:
					  return IPlatformString::createWithUTF8String("Rectangle");
                  case 1:
                      return IPlatformString::createWithUTF8String("Hann");
                  case 2:
                      return IPlatformString::createWithUTF8String("Hamming");
                  case 3:
                      return IPlatformString::createWithUTF8String("Blackman-Harris");
                  default:
                      return IPlatformString::createWithUTF8String("<Not defined>");
              }
            });
            listControl->setDrawer(listControlDrawer);
        }
        return DelegationController::verifyView(view, attributes, description);
    }
};

class ColorMapController: public DelegationController {
private:
	CVulkanView* _vulkanView;
public:
    explicit ColorMapController(IController * parent, CVulkanView *vulkanView) : DelegationController(parent),  _vulkanView(vulkanView) {}

    CView *verifyView(CView *view, const UIAttributes &attributes,
                      const IUIDescription *description) override {
        if (auto listControl = dynamic_cast<CListControl*>(view)) {
            auto * listControlDrawer = dynamic_cast<StringListControlDrawer *>(listControl->getDrawer());
            listControlDrawer->setStringProvider([](int32_t row) {
              switch(row) {
                  case 0:
                      return IPlatformString::createWithUTF8String("Magma");
				  case 1:
                      return IPlatformString::createWithUTF8String("Inferno");
                  case 2:
                      return IPlatformString::createWithUTF8String("Plasma");
				  case 3:
                      return IPlatformString::createWithUTF8String("Viridis");
                  default:
                      return IPlatformString::createWithUTF8String("<Not defined>");
              }
            });
            listControl->setDrawer(listControlDrawer);
			listControl->registerControlListener(this);
        }
        return DelegationController::verifyView(view, attributes, description);
    }

	void valueChanged(CControl *pControl) override {
        _vulkanView->setVariable("colormap", pControl->getValue());
        _vulkanView->setVariable("colormapHasChanged", true);
	}
};

class ZScaleController: public DelegationController {
private:
	CVulkanView* _vulkanView;
public:
    explicit ZScaleController(IController * parent, CVulkanView *vulkanView) : DelegationController(parent),  _vulkanView(vulkanView) {}

    CView *verifyView(CView *view, const UIAttributes &attributes,
                      const IUIDescription *description) override {
        if (auto control = dynamic_cast<CControl*>(view)) {
			control->registerControlListener(this);
			valueChanged(control);
		}
        return DelegationController::verifyView(view, attributes, description);
	}

	void valueChanged(CControl *pControl) override {
        _vulkanView->setVariable("zScale", pControl->getValue());
	}
};
//-----------------------------------------------------------------------------
tresult PLUGIN_API PlugController::initialize (FUnknown* context)
{
	tresult result = EditController::initialize (context);
	if (result == kResultTrue)
	{
		vulkanView = nullptr;
		//---Create Parameters------------

		parameters.addParameter(STR16("Z Scale"), nullptr, 0., 0.0,
								Vst::ParameterInfo::kNoFlags, VulkanVSTParams::kParamZScale, 0);

        auto * slp1 = new Vst::StringListParameter(STR16("Sampling Size"), VulkanVSTParams::kParamSamplingSize);
		for(unsigned int i = 0; i < 8; ++i) {
			slp1->appendString(std::to_wstring(64u << i).c_str());
		}
        slp1->setNormalized(5.f/7.f);
		parameters.addParameter(slp1);

        auto *slp2 = new Vst::StringListParameter(STR16("Sampling Window"), VulkanVSTParams::kParamSamplingWindow);
		slp2->appendString(STR16("Rectangle"));
		slp2->appendString(STR16("Hann"));
		slp2->appendString(STR16("Hamming"));
		slp2->appendString(STR16("Blackman-Harris"));
        slp2->setNormalized(0.25f);
		parameters.addParameter(slp2);
	}

	return result;
}

//------------------------------------------------------------------------
IPlugView* PLUGIN_API PlugController::createView (const char* name)
{
	// someone wants my editor
	if (name && strcmp (name, "editor") == 0)
	{
		auto* view = new VST3Editor (this, "view", "plug.uidesc");
		return view;

	}
	return nullptr;
}

//------------------------------------------------------------------------
tresult PLUGIN_API PlugController::setComponentState (IBStream* state)
{
	// we receive the current state of the component (processor part)
	// we read our parameters and bypass value...
	if (!state)
		return kResultFalse;

    IBStreamer streamer (state, kLittleEndian);
    int32 savedSamplingSize = 2048;
	if (!streamer.readInt32(savedSamplingSize)) {
		return kResultFalse;
	}
	int power;
	for(power = 0; savedSamplingSize > 64; power++) {
		savedSamplingSize >>= 1;
	}

	setParamNormalized(kParamSamplingSize, (float)power/7.f);

	int16 savedSamplingWindow = 1;
	if (!streamer.readInt16(savedSamplingWindow)) {
		return kResultFalse;
	}
	setParamNormalized(kParamSamplingWindow, (float)savedSamplingWindow/3.f);

    return kResultOk;
}

CView *PlugController::createCustomView(UTF8StringPtr name,
										const UIAttributes &attributes,
										const IUIDescription *description,
										VST3Editor *editor)
{
    const std::string *viewName = attributes.getAttributeValue(UIDescription::kCustomViewName);
    if (viewName != nullptr) {
        if (*viewName == "VulkanView0") {
			vulkanView = new CustomVulkanView(CRect(0,0,0,0));
			vulkanView->_callback = [this](){this->vulkanView = nullptr;};
			return vulkanView;
        }
    }
	return nullptr;
}
IController *
PlugController::createSubController(UTF8StringPtr name, const IUIDescription *description, VST3Editor *editor) {
	auto controllerName = UTF8StringView(name);
    if (controllerName == "FFTSizeController") {
		return new FFTSizeController(editor);
	} else
		if (controllerName == "FFTWindowController") {
		return new FFTWindowController(editor);
	} else
		if (controllerName == "ColorMapController") {
		return new ColorMapController(editor, vulkanView);
	} else
		if (controllerName == "ZScaleController") {
		return new ZScaleController(editor, vulkanView);
	}
	return nullptr;
}

//------------------------------------------------------------------------
tresult PLUGIN_API PlugController::notify(Vst::IMessage *message) {
    if (!message)
        return kInvalidArgument;

    if (!strcmp (message->getMessageID (), "Spectrogram"))
    {
		int64 size;
		uint32 dataSize;
		if (message->getAttributes()->getInt("Size", size) == kResultOk) {
			void *value;
			if (message->getAttributes()->getBinary("Spectrum", (const void *&)value, (uint32 &)dataSize) == kResultOk) {
				if (vulkanView && vulkanView->isAttached()) {
					vulkanView->setVariable("spectrum", (float*)value);
					vulkanView->setVariable("size", size);
					return kResultOk;
				}
			}
		}
    }

    return Vst::ComponentBase::notify (message);
}

//------------------------------------------------------------------------
} // namespace Steinberg
