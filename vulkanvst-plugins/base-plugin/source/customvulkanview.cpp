//
// Created by Benjamin on 27/11/2020.
//

#include "../include/customvulkanview.h"
#include <algorithm>

using namespace VSTGUI;
CustomVulkanView::CustomVulkanView(const CRect &rect) : CVulkanView(rect), _mouseHoldState(false), _zoom() {}

bool CustomVulkanView::attached(CView *parent) {
    if (CVulkanView::attached(parent)) {
        reset();
		return true;
	}
	return false;
}

CMouseEventResult CustomVulkanView::onMouseDown(CPoint &where, const CButtonState &buttons) {
	if (buttons.isControlSet() && buttons.isLeftButton()) {
		reset();
	}
	startMouseClickDrag(where);
	return CMouseEventResult::kMouseEventHandled;
}

CMouseEventResult CustomVulkanView::onMouseUp(CPoint &where, const CButtonState &buttons) {
	endMouseClickDrag();
	return CMouseEventResult::kMouseEventHandled;
}
CMouseEventResult CustomVulkanView::onMouseMoved(CPoint &where, const CButtonState &buttons) {
    if (!_mouseHoldState) { return CMouseEventResult::kMouseEventNotHandled; }
	_currentCursorPosition = where;
    CPoint drag = getMouseClickDragValue();
	_lastCursorPosition = _currentCursorPosition;

    if ((buttons.isAltSet() && buttons.isLeftButton()) || buttons.isMiddleButton()) {
        _cameraTranslationDrag += drag;
        setVariable("xTranslationDrag", _zoom*0.001f*(float)_cameraTranslationDrag.x);
        setVariable("yTranslationDrag", _zoom*0.001f*(float)_cameraTranslationDrag.y);
    } else if (buttons.isLeftButton()) {
		_cameraRotationDrag += drag;
		_cameraRotationDrag.y = std::clamp(_cameraRotationDrag.y, 1.0, 89.0);
		setVariable("xRotationDrag", -_cameraRotationDrag.x);
		setVariable("yRotationDrag", _cameraRotationDrag.y);
	}

	return CMouseEventResult::kMouseEventHandled;
}

bool CustomVulkanView::onWheel(const CPoint &where,
							   const CMouseWheelAxis &axis,
							   const float &distance,
							   const CButtonState &buttons) {
	if (axis == kMouseWheelAxisY) {
		_zoom -= distance*_zoom/7.0f;
		_zoom = std::clamp(_zoom, 0.5f, 5.0f);
		setVariable("zoom", _zoom);
	}
	return CView::onWheel(where, axis, distance, buttons);
}
void CustomVulkanView::startMouseClickDrag(const CPoint &startPosition) {
    _mouseHoldState = true;
	_currentCursorPosition = _lastCursorPosition = startPosition;
}

void CustomVulkanView::endMouseClickDrag() {
    _mouseHoldState = false;
}

CPoint CustomVulkanView::getMouseClickDragValue() {
	CPoint drag(_currentCursorPosition - _lastCursorPosition);
	return drag;
}

void CustomVulkanView::reset() {
    _cameraRotationDrag = {180, 20};
	_cameraTranslationDrag = {0, 0};
    _zoom = 1.1f;
    setVariable("xRotationDrag", _cameraRotationDrag.x);
    setVariable("yRotationDrag", _cameraRotationDrag.y);
    setVariable("xTranslationDrag", _cameraTranslationDrag.x);
    setVariable("yTranslationDrag", _cameraTranslationDrag.y);
    setVariable("zoom", _zoom);
	setVariable("colormapHasChanged", true);
}