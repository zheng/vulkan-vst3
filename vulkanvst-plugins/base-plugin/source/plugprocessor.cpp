//------------------------------------------------------------------------
// Project     : VulkanVST3
//
// Category    : Examples
// Filename    : plugprocessor.cpp
// Created by  : Steinberg, 01/2018
// Based on    : HelloWorld Example for VST 3
// Description : A Vulkan VST 3 example
//
//-----------------------------------------------------------------------------
// LICENSE
// (c) 2020, Steinberg Media Technologies GmbH, All Rights Reserved
//-----------------------------------------------------------------------------
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
//   * Redistributions of source code must retain the above copyright notice, 
//     this list of conditions and the following disclaimer.
//   * Redistributions in binary form must reproduce the above copyright notice,
//     this list of conditions and the following disclaimer in the documentation 
//     and/or other materials provided with the distribution.
//   * Neither the name of the Steinberg Media Technologies nor the names of its
//     contributors may be used to endorse or promote products derived from this 
//     software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
// IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
// OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

#include "../include/plugprocessor.h"

#include "../include/plugids.h"
#include "../include/window.h"
#include "../../../src/kspectrogram.hpp"
#include "base/source/fstreamer.h"
#include "pluginterfaces/base/ibstream.h"
#include "pluginterfaces/vst/ivstparameterchanges.h"

//#include "ipp.h"
#include <ipp.h>

#include <algorithm>

namespace Steinberg {
namespace VulkanVST {

//-----------------------------------------------------------------------------
PlugProcessor::PlugProcessor () : rbuffer(Spectrogram::SpectrumSize*3), nbSpectrums(0)
{
	// register its editor class
	setControllerClass (MyControllerUID);
}

//-----------------------------------------------------------------------------
tresult PLUGIN_API PlugProcessor::initialize (FUnknown* context)
{
	//---always initialize the parent-------
	tresult result = AudioEffect::initialize (context);
	if (result != kResultTrue)
		return kResultFalse;

	//---create Audio In/Out buses------
	// we want a stereo Input and a Stereo Output
	addAudioInput (STR16 ("AudioInput"), Vst::SpeakerArr::kStereo);
	addAudioOutput (STR16 ("AudioOutput"), Vst::SpeakerArr::kStereo);
	return kResultTrue;
}

//-----------------------------------------------------------------------------
tresult PLUGIN_API PlugProcessor::setBusArrangements (Vst::SpeakerArrangement* inputs,
                                                            int32 numIns,
                                                            Vst::SpeakerArrangement* outputs,
                                                            int32 numOuts)
{
	// we only support one in and output bus and these buses must have the same number of channels
	if (numIns == 1 && numOuts == 1 && inputs[0] == outputs[0])
	{
		return AudioEffect::setBusArrangements (inputs, numIns, outputs, numOuts);
	}
	return kResultFalse;
}

//-----------------------------------------------------------------------------
tresult PLUGIN_API PlugProcessor::setupProcessing (Vst::ProcessSetup& setup)
{
	// here you get, with setup, information about:
	// sampleRate, processMode, maximum number of samples per audio block
	return AudioEffect::setupProcessing (setup);
}

//-----------------------------------------------------------------------------
tresult PLUGIN_API PlugProcessor::setActive (TBool state)
{
	if (state) // Initialize
	{
		// Allocate Memory Here
		// Ex: algo.create ();
        fMessage = allocateMessage();
        fMessage->setMessageID("Spectrogram");
		spectrums.resize(Spectrogram::SpectrumSize * Spectrogram::MaxSpectrums);
		ippBuffers = new IPPBuffers(Spectrogram::SpectrumSize);
		messageSenderTimer = new VSTGUI::CVSTGUITimer(
			[this](VSTGUI::CVSTGUITimer * timer) {
              this->fMessage->getAttributes()->setBinary("Spectrum", (void *)this->spectrums.data(), this->spectrums.size()*sizeof(float));
              this->fMessage->getAttributes()->setInt("Size", this->nbSpectrums);
              this->sendMessage(this->fMessage);
			  this->nbSpectrums = 0;
			},
			1000/240
			);
	}
	else // Release
	{
		// Free Memory if still allocated
		// Ex: if(algo.isCreated ()) { algo.destroy (); }
		messageSenderTimer->stop();
		fMessage->release();
		messageSenderTimer->forget();
		spectrums.clear();
		delete ippBuffers;
	}
	return AudioEffect::setActive (state);
}

//-----------------------------------------------------------------------------
tresult PLUGIN_API PlugProcessor::process (Vst::ProcessData& data)
{
	//--- Read inputs parameter changes-----------
	if (data.inputParameterChanges)
	{
		int32 numParamsChanged = data.inputParameterChanges->getParameterCount ();
		for (int32 index = 0; index < numParamsChanged; index++)
		{
			Vst::IParamValueQueue* paramQueue =
			    data.inputParameterChanges->getParameterData (index);
			if (paramQueue)
			{
				Vst::ParamValue value;
				int32 sampleOffset;
				int32 numPoints = paramQueue->getPointCount ();
				switch (paramQueue->getParameterId ())
				{
					case VulkanVSTParams::kParamSamplingSize:
                        if (paramQueue->getPoint(numPoints-1, sampleOffset, value) == kResultTrue) {
							iSamplingSize = 64u << (uint32_t)(7*value);
						}
						break;
					case VulkanVSTParams::kParamSamplingWindow:
                        if (paramQueue->getPoint(numPoints-1, sampleOffset, value) == kResultTrue) {
							iSamplingWindowType = 3*value;
						}
				}
			}
		}
	}

	//--- Process Audio---------------------
	//--- ----------------------------------
	if (data.numInputs == 0 || data.numOutputs == 0)
	{
		// nothing to do
		return kResultOk;
	}

	if (data.numSamples > 0)
	{
		// Process Algorithm
		// Ex: algo.process (data.inputs[0].channelBuffers32, data.outputs[0].channelBuffers32,
		// data.numSamples);
		Vst::SpeakerArrangement arr;
		getBusArrangement(Vst::kOutput, 0, arr);
		int32 numChannels = Vst::SpeakerArr::getChannelCount(arr);

		for(int32 channel = 0; channel < numChannels; ++channel) {
            float* inputChannel = data.inputs[0].channelBuffers32[channel];
            float* outputChannel = data.outputs[0].channelBuffers32[channel];

            data.outputs[0].silenceFlags = 0;

            for(int32 sample = 0; sample < data.numSamples; ++sample) {
                outputChannel[sample] = inputChannel[sample];
            }
		}

        if (data.inputs[0].silenceFlags != 0) {
            data.outputs[0].silenceFlags = data.inputs[0].silenceFlags;
            for(int32 channel = 0; channel < numChannels; ++channel) {
                float* inputChannel = data.inputs[0].channelBuffers32[channel];
                float* outputChannel = data.outputs[0].channelBuffers32[channel];
                if (inputChannel != outputChannel) {
                    memset(outputChannel, 0, data.numSamples*sizeof(int32));
                }
            }
        }


		//Calculate spectrogram when first channel is not silent
		if ((data.inputs[0].silenceFlags & 1u) == 0) {
			data.outputs[0].silenceFlags = data.inputs[0].silenceFlags;

			auto spectrumSize = Spectrogram::SpectrumSize;

			for (int i = 0; i < data.numSamples; i++) {
				if (nbSpectrums >= 256) {
					continue;
				}
                rbuffer._buffer[rbuffer._tail] = data.inputs[0].channelBuffers32[0][i];
                rbuffer._tail++;
 				if (rbuffer.getEffectiveSize() == iSamplingSize) {
					// calculate spectrum
					float *spectrum = calculateSpectrum(rbuffer, spectrumSize, iSamplingWindowType);
					memcpy(spectrums.data() + nbSpectrums * spectrumSize, spectrum, spectrumSize * sizeof(float));

					rbuffer._head += iStepSize;
					if (rbuffer._head >= rbuffer._size) { rbuffer._head = rbuffer._head - rbuffer._size; }
					nbSpectrums++;
				}
                if (rbuffer._tail == rbuffer._size) {
                    rbuffer._tail = 0;
                }
			}
		}
    }
	return kResultOk;
}

//------------------------------------------------------------------------
tresult PLUGIN_API PlugProcessor::setState (IBStream* state)
{
	if (!state)
		return kResultFalse;

	// called when we load a preset or project, the model has to be reloaded
    IBStreamer streamer(state, kLittleEndian);
    int32 toSaveSamplingSize = 0u;
    int16 toSaveSamplingWindowType = 0u;

    if (!streamer.readInt32(toSaveSamplingSize)
		|| !streamer.readInt16(toSaveSamplingWindowType)) {
		return kResultFalse;
	}

	iSamplingSize = toSaveSamplingSize;
	iSamplingWindowType = toSaveSamplingWindowType;

    return kResultOk;
}

//------------------------------------------------------------------------
tresult PLUGIN_API PlugProcessor::getState (IBStream* state)
{
	// here we need to save the model (preset or project)
	int32 toSaveSamplingSize = iSamplingSize;
	int16 toSaveSamplingWindowType = iSamplingWindowType;

	IBStreamer streamer(state, kLittleEndian);
	streamer.writeInt32(toSaveSamplingSize);
	streamer.writeInt16(toSaveSamplingWindowType);

	return kResultOk;
}

float *PlugProcessor::calculateSpectrum(const RingBuffer &data, long size, int windowType) const {
	long dataSize = data.getEffectiveSize();

    IppsFFTSpec_C_32fc *pFFTSpec = nullptr;

	int current = data._head;
	for (long i = 0; i < size*2; i++) {
        if (i < dataSize) {
            if (current >= data._size) {
                current = 0;
            }
            ippBuffers->pSrc[i].re = data._buffer[current];
            current++;
        } else {
            ippBuffers->pSrc[i].re = 0;
        }
        ippBuffers->pSrc[i].im = 0;
        ippBuffers->pDst[i].re = 0;
        ippBuffers->pDst[i].im = 0;
	}

    applyWindowFuntion(windowType, reinterpret_cast<float *>(ippBuffers->pSrc), dataSize);

    ippsFFTInit_C_32fc(&pFFTSpec, ippBuffers->order, IPP_FFT_NODIV_BY_ANY, ippAlgHintAccurate, ippBuffers->pFFTSpecBuf, ippBuffers->pFFTInitBuf);
    ippsFFTFwd_CToC_32fc(ippBuffers->pSrc, ippBuffers->pDst, pFFTSpec, ippBuffers->pFFTWorkBuf);

    ippsMagnitude_32fc(ippBuffers->pDst, ippBuffers->pMagnitude, size);

    return ippBuffers->pMagnitude;
}

//------------------------------------------------------------------------
PlugProcessor::IPPBuffers::IPPBuffers(long outputSize) {
    long workSize = outputSize*2;

    order = (int)log2(workSize);

    pSrc = ippsMalloc_32fc(workSize);
    pDst = ippsMalloc_32fc(workSize);
	//Zero-padding
    for (long i = outputSize; i < workSize; i++) {
        pSrc[i].re = 0;
        pSrc[i].im = 0;
        pDst[i].re = 0;
        pDst[i].im = 0;
    }

    int sizeFFTSpec, sizeFFTInitBuf, sizeFFTWorkBuf;
	ippsFFTGetSize_C_32fc(order,
                          IPP_FFT_NODIV_BY_ANY,
                          ippAlgHintAccurate,
                          &sizeFFTSpec,
                          &sizeFFTInitBuf,
                          &sizeFFTWorkBuf);

    pFFTSpecBuf = ippsMalloc_8u(sizeFFTSpec);
    pFFTInitBuf = ippsMalloc_8u(sizeFFTInitBuf);
    pFFTWorkBuf = ippsMalloc_8u(sizeFFTWorkBuf);
    pMagnitude = ippsMalloc_32f(outputSize);
}

PlugProcessor::IPPBuffers::~IPPBuffers() {
    if (pFFTInitBuf) { ippFree(pFFTInitBuf); }
    if (pFFTWorkBuf) { ippFree(pFFTWorkBuf); }
    if (pFFTSpecBuf) { ippFree(pFFTSpecBuf); }
    ippFree(pSrc);
    ippFree(pDst);
	ippFree(pMagnitude);
}

} // namespace VulkanVST
} // namespace Steinberg
