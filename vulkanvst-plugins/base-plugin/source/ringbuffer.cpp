#include "../include/ringbuffer.h"

RingBuffer::RingBuffer(long size) {
	_buffer = new float[size]; 
	_size = size;
	_head = 0;
	_tail = 0;
}

long RingBuffer::getEffectiveSize() const {
	if (_head < _tail) {
		return _tail - _head;
	}
	return _tail - (_head - _size);
}

