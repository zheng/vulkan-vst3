#define _USE_MATH_DEFINES
#include <cmath>
#include "../include/window.h"

void applyWindowFuntion(int windowType, float *signal, int size) {
	switch (windowType) { 
	case 0: //Rect Window (nothing to do)
		break;
	case 1: //Hamming Window
		for (int i = 0; i < size; i++) {
			signal[i] = signal[i] * (0.54 - 0.46*sin(2*M_PI*i/size));
		}
		break;
	case 2: //Hann Window
		for (int i = 0; i < size; i++) {
			signal[i] = signal[i] * (0.5 - 0.5 * sin(2*M_PI*i/size));
		}
		break;
	case 3: //Blackman Window 
		for (int i = 0; i < size; i++) {
			signal[i] = signal[i] * (0.42 - 0.5*sin(2 * M_PI * i / size) + 0.08*sin(4 * M_PI * i / size));
		}
		break;
	default:
		break;
	}
}