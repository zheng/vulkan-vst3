#pragma once

class RingBuffer {
public:
	RingBuffer(long size);
	~RingBuffer() { delete _buffer; };
	long getEffectiveSize() const;
	float *_buffer;
	long _head;
	long _tail;
	long _size;
};