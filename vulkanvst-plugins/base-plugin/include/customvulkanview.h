#ifndef VULKANVST3_CUSTOMVULKANVIEW_H
#define VULKANVST3_CUSTOMVULKANVIEW_H

#include "../../vulkan-view/include/cvulkanview.hpp"

#include <vstgui/lib/crect.h>

namespace VSTGUI {
class CustomVulkanView: public CVulkanView {
public:
	CustomVulkanView(const CRect &);
	CMouseEventResult onMouseDown(CPoint &where, const CButtonState &buttons) override;
	CMouseEventResult onMouseUp(CPoint &where, const CButtonState &buttons) override;
	CMouseEventResult onMouseMoved(CPoint &where, const CButtonState &buttons) override;
    bool onWheel(const CPoint &where,
                 const CMouseWheelAxis &axis,
                 const float &distance,
                 const CButtonState &buttons) override;

	bool attached(CView *parent) override;

	void reset();

private:
	void startMouseClickDrag(const CPoint &startPosition);
	void endMouseClickDrag();
	CPoint getMouseClickDragValue();

private:
	bool _mouseHoldState;
	CPoint _currentCursorPosition;
	CPoint _lastCursorPosition;
	CPoint _cameraRotationDrag;
	CPoint _cameraTranslationDrag;
	float _zoom;
};
}

#endif // VULKANVST3_CUSTOMVULKANVIEW_H
